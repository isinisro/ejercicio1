import {LitElement, html, css } from 'lit-element';

class TestBootstrap extends LitElement {

    static get styles() {
        return css`
        .redbg {
            background-color: red;
        }

        .greenbg {
            background-color: green;
        }
        
        .bluebg {
            background-color: blue;
        }
        
        .greybg {
            background-color: grey;
        }
        `;
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <h3>Test Bootstrap</h3>
        <div class="row greybg">
            <div class="col-2 offset-1 redbg">Col 1</div>
            <div class="col-3 offset-2 greenbg">Col 2</div>
            <div class="col-2 bluebg">Col 3</div>
        </div>
        `;
    }
}

customElements.define("test-bootstrap", TestBootstrap);