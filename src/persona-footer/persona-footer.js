import {LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

static get properties() {

    return {

    };
}

constructor(){
    super();
}

    render() {
        return html`
            <div>Persona APP 2021</div>
        `;
    }
}

customElements.define("persona-footer", PersonaFooter);