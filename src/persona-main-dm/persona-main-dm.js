import { LitElement, html } from "lit-element";

class PersonaMainDM extends LitElement{

    static get properties(){
        return{
            people:{type: Array},
        };
    }

    constructor(){
        super();
        this.people = [
            {
                name: "Zabala",
                yearsInCompany: 10,
                photo: {
                    src: "./img/zabala.png",
                    alt: "Zabala"
                },
                profile: "General de la Vanguardia."                
            },
            {
                name: "Ikora Rey",
                yearsInCompany: 9,
                photo: {
                    src: "./img/image.png",
                    alt: "Teniente de la Nueva"
                },
                profile: "Teniente de la Nueva Monarquía"                                
            },
            {
                name: "Banshee 44",
                yearsInCompany: 5,
                photo: {
                    src: "./img/Banshee-44.png",
                    alt: "Banshee 44"
                },
                profile: "Responsable de la armería."                             
            },
            {
                name: "Cayde 7",
                yearsInCompany: 4,
                photo: {
                    src: "./img/cayde7.png",
                    alt: "Cayde 7"
                },
                profile: "El hombre a quien llaman Cayde. Pero este es el secreto con los errores: aprendes de ellos. De nuevo, esto es así si asumimos el hipotético caso de que verdaderamente yo haya cometido algunos errores. Así que, sí, puede ser que eso es lo que estoy haciendo. Intentando aprender de estos muy hipotéticos deslices. Le llaman mirar hacia adentro. Eris le dice de otra forma. Eris denomina a un montón de cosas de manera completamente diferente. RIP"
            },
            {
                name: "Desconocida",
                yearsInCompany: 1,
                photo: {
                    src: "./img/desconocida.png",
                    alt: "Desconocida"
                },
                profile: "Cazarecompensas."                                
            }
        ]
    }

    updated(changedProperties) {
        console.log("updated");

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de people en persona-main-dm");

            this.dispatchEvent(new CustomEvent("people-data-updated", {
                detail: {
                    people: this.people
                }
            }))
        }
    }
}  
customElements.define("persona-main-dm", PersonaMainDM);