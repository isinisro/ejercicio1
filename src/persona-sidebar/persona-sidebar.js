import {LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {

static get properties() {

    return {
        peopleStats: {type: Object}
    };
}

constructor(){
    super();
    this.peopleStats = {};
}

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <aside>
            <section>
                <h3>
                    Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
                </h3>
                <div class="mt-5">
                    <button @click="${this.newPerson}" class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong></button>
                </div>
                <div class="mt-5">
                <div>
                    <input type="range" min"0"
                    max="${this.peopleStats.maxYearsInCompany}"
                    step="1"
                    value="${this.peopleStats.maxYearsInCompany}"
                    @input="${this.updateMaxYearsInCompanyFilter}"
                    />
                </div>
                </div>             
            </section>
        </aside>
        `;
    }

    updateMaxYearsInCompanyFilter(e) {
        console.log("updateMaxYearsInCompanyFilter");
        console.log("El range vale " + e.target.value);

        this.dispatchEvent(
            new CustomEvent(
                "updated-max-years-filter",
                {
                    detail: {
                        maxYearsInCompany: e.target.value
                    }
                }
            )
        );
    }
    
    newPerson(e) {
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent("new-person", {}));
    }
}

customElements.define("persona-sidebar", PersonaSidebar);