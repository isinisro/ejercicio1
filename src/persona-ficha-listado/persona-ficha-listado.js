import { LitElement, html } from "lit-element";

class PersonaFichaListado extends LitElement{

    static get properties(){
        return{
            fname: {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object},
            profile: {type: String}

        };
    }

    constructor(){
        super();
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div class="card h-100">
                <img src="${this.photo.src}" alt="${this.photo.alt}" 
                height="300" width="150" class="card-img-top">
                <div class="card-body">
                    <h4 class="card-title">${this.fname}</h4>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.yearsInCompany} años en la Vanguardia</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>X</strong></button>
                    <button @click="${this.moreInfo}" class="btn btn-info offset-1 col-5"><strong>Info</strong></button>
                </div>
            </div>    
        `;
    }

    moreInfo(e) {
        console.log("moreInfo");
        console.log("se ha pedido información de la persona " + this.fname);
        
        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    name: this.fname
                }
            })
        );
    }

    deletePerson(e) {
        console.log("deletePerson en persona-ficha-listado");
        console.log("se va a borrar la persona de nombre " + this.fname);

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                detail: {
                    name: this.fname
                }
            })
        );
    }
}

customElements.define("persona-ficha-listado", PersonaFichaListado);

