import {LitElement, html } from 'lit-element';

class PersonaHeader extends LitElement {

static get properties() {

    return {

    };
}

constructor(){
    super();
}

    render() {
        return html`
            <h1>Héroes de la Vanguardia</h1>
        `;
    }
}

customElements.define("persona-header", PersonaHeader);